package stepDefinitions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import seleniumPages.Page_ToDoApp;

public class stepDefsTask {

	Page_ToDoApp todoappHomepage = new Page_ToDoApp();

	//=======================================================================================
	//==================senario 1============================================================

	@Given("^User navegate to \"(.*?)\"$")
	public void i_navegate_to(String arg1) {
		todoappHomepage.launchBrowser();
		todoappHomepage.openToDoApp();
	}
	@Given("^Sing in with user \"(.*?)\"$")
	public void sing_in_with_user(String arg1) {
		todoappHomepage.typeUserEmail();
	}

	@Given("^Password \"(.*?)\"$")
	public void password(String arg1)  {
		todoappHomepage.typePasswd();
	}

	@Given("^Click on Sing in$")
	public void click_on_Sing_in() {
		todoappHomepage.clickSinginButton();
	}

	@Then("^User be able to login successfully$")
	public void user_be_able_to_login_successfully() {
		todoappHomepage.assertSingedSuccessfully();
		todoappHomepage.closeBrouser();
	}

	//=======================================================================================
	//==================senario 2============================================================	

	@Given("^User is loggad in$")
	public void user_is_loggad_in() {
		todoappHomepage.launchBrowser();
		todoappHomepage.openToDoApp();
		todoappHomepage.typeUserEmail();
		todoappHomepage.typePasswd();
		todoappHomepage.clickSinginButton();
	}

	@When("^User should always see the My Tasks link on the NavBar$")
	public void user_should_always_see_the_My_Tasks_link_on_the_NavBar() {
		todoappHomepage.assertMyTaskButun();
	}

	@When("^Click on My Tasks$")
	public void click_on_My_Tasks() {
		todoappHomepage.clickMyTaskButton(); 
	}

	@Then("^User should see a message on the top part saying that list belongs to the logged user Welcome, \"(.*?)\"!$")
	public void user_should_see_a_message_on_the_top_part_saying_that_list_belongs_to_the_logged_user_Welcome(String arg1) {
		todoappHomepage.assertDsplyluMsg();
		todoappHomepage.closeBrouser();	
	}

	//=======================================================================================
	//==================senario 3============================================================	

	@When("^User add a new task \"(.*?)\"$")
	public void user_add_a_new_task(String arg1) {
		todoappHomepage.clickMyTaskButton();
		todoappHomepage.typeTaskText();
		todoappHomepage.clickNewTaskButton();
	}
	@Then("^The new task \"(.*?)\" has added on the list of created tasks$")
	public void the_new_task_has_added_on_the_list_of_created_tasks(String arg1) {		
		todoappHomepage.assertAddedTask();
		todoappHomepage.clickRemoveTaskButton();
		todoappHomepage.closeBrouser();
	}

	//=======================================================================================
	//==================senario 4============================================================
	@Given("^User enter more then (\\d+) characters$")
	public void user_enter_more_then_characters(int arg1)  {
		todoappHomepage.launchBrowser();
		todoappHomepage.openToDoApp();
		todoappHomepage.typeUserEmail();
		todoappHomepage.typePasswd();
		todoappHomepage.clickSinginButton();
		todoappHomepage.clickMyTaskButton();
		todoappHomepage.typeTaskText250char();
		todoappHomepage.clickNewTaskButton();
	}

	@Then("^The task cant have more than (\\d+) characters\\.$")
	public void the_task_cant_have_more_than_characters(int arg1) throws Throwable {
		todoappHomepage.assert250cahr();
		todoappHomepage.clickRemoveTaskButton();
		todoappHomepage.closeBrouser();
	}

	//=======================================================================================
	//==================senario 5============================================================	
	@Given("^User enter less then (\\d+) characters$")
	public void user_enter_less_then_characters(int arg1)  {
		todoappHomepage.launchBrowser();
		todoappHomepage.openToDoApp();
		todoappHomepage.typeUserEmail();
		todoappHomepage.typePasswd();
		todoappHomepage.clickSinginButton();
		todoappHomepage.clickMyTaskButton();
		todoappHomepage.typeTaskTextLess3char();
		todoappHomepage.clickNewTaskButton();

	}

	@Then("^The task should require at least three characters so the user can enter it\\.$")
	public void the_task_should_require_at_least_three_characters_so_the_user_can_enter_it() {
		todoappHomepage.assertLess3cahr();
		todoappHomepage.clickRemoveTaskButton();
		todoappHomepage.closeBrouser();

	}
}