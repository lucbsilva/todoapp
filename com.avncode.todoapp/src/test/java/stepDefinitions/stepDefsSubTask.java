package stepDefinitions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import seleniumPages.Page_ToDoApp;

public class stepDefsSubTask {

	Page_ToDoApp todoappHomepage = new Page_ToDoApp();

	//=======================================================================================
	//==================senario 1============================================================
	@Given("^User have a task$")
	public void user_have_a_task() {
		todoappHomepage.launchBrowser();
		todoappHomepage.openToDoApp();
		todoappHomepage.typeUserEmail();
		todoappHomepage.typePasswd();
		todoappHomepage.clickSinginButton();
		todoappHomepage.clickMyTaskButton();
		todoappHomepage.typeTaskText();
		todoappHomepage.clickNewTaskButton();
	}

	@When("^User see a button labeled as Manage Subtasks$")
	public void user_see_a_button_labeled_as_Manage_Subtasks()  {
		todoappHomepage.assertButtonlabeledManageSubtasks();
	}

	@When("^This button have the number of subtasks created for that tasks$")
	public void this_button_have_the_number_of_subtasks_created_for_that_tasks()  {
		todoappHomepage.assertButtonlabeledManageSubtasks();
	}

	@When("^Click in the button Manage Subtasks$")
	public void click_in_the_button_Manage_Subtasks()  {
		todoappHomepage.clickButtonManageSubtasks();
	}

	@Then("^Opens up a modal dialog have a read only field with the task ID and the task description$")
	public void opens_up_a_modal_dialog_have_a_read_only_field_with_the_task_ID_and_the_task_description()  {
		todoappHomepage.assertModalLabel();
		todoappHomepage.assertTaskDescritionSubtaskAtribute();
	}

	@Then("^enter the SubTask Description \\((\\d+) characters\\)$")
	public void enter_the_SubTask_Description_characters(int arg1)  {
		todoappHomepage.typeSubTaskText250char();
	}

	@Then("^SubTask due date \\(MM/dd/yyyy format\\)$")
	public void subtask_due_date_MM_dd_yyyy_format()  {
		todoappHomepage.dueDateFormat();
	}

	@Then("^Click on the add button to add a new Subtask$")
	public void click_on_the_add_button_to_add_a_new_Subtask()  {
		todoappHomepage.clickAddSubtask();
	}

	@Then("^Subtasks that were added should be appended on the bottom part of the modal dialog$")
	public void subtasks_that_were_added_should_be_appended_on_the_bottom_part_of_the_modal_dialog()  {
		todoappHomepage.assertAddedTask();
		todoappHomepage.clickCloseSubTasck();
		todoappHomepage.clickRemoveTaskButton();
		todoappHomepage.closeBrouser();
	}

	//=======================================================================================
	//==================senario 2============================================================

	@When("^The Task Description are required field$")
	public void the_Task_Description_are_required_field() {
		todoappHomepage.clickButtonManageSubtasks();		
		todoappHomepage.clearTaskDescription();
	}

	@When("^Due Date are required fields$")
	public void due_Date_are_required_fields() {
		todoappHomepage.clearDueDate();    
	}

	@Then("^The system informs the field are required$")
	public void the_system_informs_the_field_are_required() {
		todoappHomepage.clickAddSubtask();
		//todoappHomepage.assertEmptyFielsSubTask();
		todoappHomepage.clickCloseSubTasck();
		todoappHomepage.clickRemoveTaskButton();
		todoappHomepage.closeBrouser();
	}
}
