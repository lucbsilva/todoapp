package seleniumPages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;

import common.Page_BasePage;

public class Page_ToDoApp extends Page_BasePage {

	public void launchBrowser() {
		System.setProperty(
				"webdriver.chrome.driver",
				"C:\\Users\\luabs\\eclipse-workspace\\com.avncode.todoapp\\src\\test\\resources\\chromedriver.exe"
				);
		driver = new ChromeDriver();
		driver.manage().window().maximize();			
	}

	public void openToDoApp() {
		driver.get("https://qa-test.avenuecode.com/users/sign_in");
	}

	public void typeUserEmail() {
		driver.findElement(
				By.id("user_email")).sendKeys("luabsilva@gmail.com");
	}

	public void typePasswd() {
		driver.findElement(
				By.id("user_password")).sendKeys("AvnCode001");
	}

	public void clickSinginButton() {
		driver.findElement(
				By.name("commit")).click();
	}

	public void assertSingedSuccessfully() {
		Assert.assertTrue(
				driver.getPageSource().contains("Signed in successfully."));
	}

	public void closeBrouser() {
		driver.close();
	}

	public void assertMyTaskButun() {
		Assert.assertTrue(
				driver.getPageSource().contains("btn btn-lg btn-success"));
	}

	public void clickMyTaskButton() {
		driver.findElement(
				By.linkText("My Tasks")).click();
	}

	public void assertDsplyluMsg() {
		Assert.assertTrue(
				driver.getPageSource().contains("Welcome, luciano!"));
	}

	public void typeTaskText() {

		driver.findElement(
				By.id("new_task")).sendKeys("test");
	}

	public void typeTaskText250char() {
		driver.findElement(
				By.id("new_task")).sendKeys(
						"12345678901234567890123456789012345678901234567890" //50 char
						+ "12345678901234567890123456789012345678901234567890" //50 char
						+ "12345678901234567890123456789012345678901234567890" //50 char
						+ "12345678901234567890123456789012345678901234567890" //50 char
						+ "12345678901234567890123456789012345678901234567890123456"
						); //56 char
	}

	public void assert250cahr() { // bug reported 
		Assert.assertTrue(
				driver.getPageSource().contentEquals(
						"12345678901234567890123456789012345678901234567890"   //50 char
						+ "12345678901234567890123456789012345678901234567890" //50 char
						+ "12345678901234567890123456789012345678901234567890" //50 char
						+ "12345678901234567890123456789012345678901234567890" //50 char
						+ "12345678901234567890123456789012345678901234567890" //50 char
						)

				); 
	}

	public void typeTaskTextLess3char() {

		driver.findElement(
				By.id("new_task")).sendKeys("12");
	}

	public void assertLess3cahr() {
		Assert.assertTrue(
				driver.getPageSource().contentEquals("inform three or more characters")); // bug reported
	}

	public void clickNewTaskButton() {
		driver.findElement(
				By.xpath("//div[2]/span")).click();
	}

	public void clickRemoveTaskButton() {
		driver.findElement(
				By.xpath("//td[5]/button")).click();
	}

	public void assertAddedTask() {
		Assert.assertTrue(
				driver.getCurrentUrl().contains("test"));
	}
	public void clickCloseSubTasck() {
		driver.findElement(
				By.xpath("//div/div[3]/button")).click();
	}
	
	public void assertButtonlabeledManageSubtasks() {
		Assert.assertTrue(
				driver.getPageSource().contains("(0) Manage Subtasks"));
	}

	public void clickButtonManageSubtasks() {
		driver.findElement(
				By.xpath("//td[4]/button")).click();
	}

	public void assertModalLabel() {
		Assert.assertTrue(
				driver.getPageSource().contains("Editing Task"));
	}

	public void typeSubTaskText250char() {
		driver.findElement(
				By.id("new_sub_task")).sendKeys(
						"12345678901234567890123456789012345678901234567890" //50 char
						+ "12345678901234567890123456789012345678901234567890" //50 char
						+ "12345678901234567890123456789012345678901234567890" //50 char
						+ "12345678901234567890123456789012345678901234567890" //50 char
						+ "123456789012345678901234567890123456789012345678901"); //51 char
	}

	public void assertTaskDescritionSubtaskAtribute() {
		driver.findElement(
				By.name("edit_task")).sendKeys("fild is not read only");		
	}

	public void dueDateFormat() {
		driver.findElement(
				By.name("due_date")).sendKeys("13/12/2018");
	}

	public void clickAddSubtask() {
		driver.findElement(
				By.xpath("//*[@id=\"add-subtask\"]"));
	}
	public void clearTaskDescription() {
		driver.findElement(
				By.id("edit_task")).clear();
	}

	public void clearDueDate() {
		driver.findElement(
				By.id("dueDate")).clear();
	}

	public void assertEmptyFielsSubTask() {
		Assert.assertTrue(
				driver.getPageSource().contentEquals("inform three or more characters")); // bug reported
	}
}