Feature: SubTask As a ToDo App user
	I should be able to create a subtask
	So I can break down my tasks in smaller pieces
	
	The Task Description and Due Date are required fields
		

	Scenario: Create a subtask
		Given User have a task 
		When User see a button labeled as Manage Subtasks
		And This button have the number of subtasks created for that tasks
		And Click in the button Manage Subtasks
		Then Opens up a modal dialog have a read only field with the task ID and the task description
		And enter the SubTask Description (250 characters) 
		And SubTask due date (MM/dd/yyyy format)
		And Click on the add button to add a new Subtask
		And Subtasks that were added should be appended on the bottom part of the modal dialog

	Scenario: Required fields
		Given User have a task 
		When The Task Description are required field
		And Due Date are required fields
		Then The system informs the field are required
		