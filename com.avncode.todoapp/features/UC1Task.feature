Feature: Task As a ToDo App user
	I should be able to create a task
	So I can manage my tasks

	Scenario: Sing in on ToDo App
		Given User navegate to "https://qa-test.avenuecode.com/users/sign_in"
		And Sing in with user "luabsilva@gmail.com"
		And Password "AvnCode001"
		And Click on Sing in
		Then User be able to login successfully
		
	Scenario: Assert mensages on the screen
		Given User is loggad in
		Then User should always see the My Tasks link on the NavBar
		And User should see a message on the top part saying that list belongs to the logged user Welcome, "luciano"!
	
	Scenario: Task appended on the list of created tasks.
		Given User is loggad in
		When User add a new task "test"
		Then The new task "test" has added on the list of created tasks

	Scenario: More then 250 characters
		Given  User enter more then 250 characters
		Then The task cant have more than 250 characters.		
	
	Scenario: Three characters to enter new task
		Given User enter less then 3 characters 
		Then The task should require at least three characters so the user can enter it.